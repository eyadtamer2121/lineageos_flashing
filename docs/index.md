---
hide:
  - navigation
---

# Getting started

## Welcome here :D

Greetings and welcome!

You probably got redirected here from my XDA threads or Telegram posts regarding the devices maintained by me:

* [Redmi S2/Y2 `ysl`](https://forum.xda-developers.com/t/rom-android-13-unofficial-ota-lineageos-20-0-for-xiaomi-redmi-s2-y2-ysl.4421215/)
* [Redmi 6 Pro `sakura`](https://forum.xda-developers.com/t/rom-android-13-unofficial-ota-lineageos-20-0-for-xiaomi-redmi-6-pro-sakura.4611297/)
* [Mi A2 Lite `daisy`](https://forum.xda-developers.com/t/rom-android-13-unofficial-ota-lineageos-20-0-for-xiaomi-mi-a2-lite-daisy.4421213/)
* [Xiaomi 11 Lite 5G NE `lisa`](https://forum.xda-developers.com/t/rom-android-13-official-lineageos-20-for-xiaomi-11-lite-5g-ne-mi-11-le-lisa.4468087/)

## Before proceeding

* The device trees sources used for these builds can be found on my GitHub profile [`https://github.com/ItsVixano`](https://github.com/ItsVixano) and GitLab [`https://gitlab.com/ItsVixano-dev`](https://gitlab.com/ItsVixano-dev).
* Apart from the LineageOS source, I also apply some patches on my own, which can be found [here](https://github.com/ItsVixano/android_vendor_extra).
* These builds include the support for OTA updates.
* These builds include the support for [`MicroG`](https://microg.org).
* Custom kernels are **NOT SUPPORTED, do NOT flash them**.
* Flash LineageOS **ONLY** with LineageOS Recovery.
* Make sure your PC has [`ADB & Fastboot tools`](https://developer.android.com/studio/releases/platform-tools) installed as well as the fastboot drivers.
* Make sure your device has the bootloader unlocked.
* Make sure you have made a backup of **EVERYTHING** present on your phone before proceeding.

## Support chats

I am active on both XDA threads and my own [Telegram group](https://t.me/giostuffs) or [Matrix chat](https://matrix.to/#/#giostuffs:matrix.org) if you want to ask any questions or resolve some problems.

If you read all of this, you can continue to the next page (Press the menu at the top-left part of the screen if you are on mobile) :D
