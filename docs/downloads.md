---
hide:
  - navigation
---

# Download links

## LineageOS

### ROM

The download links for LineageOS are avaible from my XDA thread / Telegram posts, or from here:

**WARNING: Official builds for [`lisa`](https://wiki.lineageos.org/devices/lisa/) are not covered in this page.**

* [Redmi S2/Y2 `ysl`](https://github.com/ItsVixano-releases/LineageOS_ysl/releases/)
* [Redmi 6 Pro `sakura`](https://github.com/ItsVixano-releases/LineageOS_sakura/releases/)
* [Mi A2 Lite `daisy`](https://github.com/ItsVixano-releases/LineageOS_daisy/releases/)
* [Xiaomi 11 Lite 5G NE `lisa`](https://github.com/ItsVixano-releases/LineageOS_lisa/releases/)

### LineageOS Recovery

With every release, I upload also `LineageOS Recovery` images:

* `recovery.img` (For `ysl` and `sakura`).
* `boot.img` (For `daisy` and `lisa`).
* `vendor_boot.img` (For `lisa`).
* `dtbo.img` (For `lisa`).

Download all of them as they are required for the flashing process.

## Firmware

For these builds, I recommend downloading the latest Global Stable firmware to ensure compatibility with the used blobs:

**WARNING: Since `LineageOS 20.0` i started to ship [firmware images](https://gitlab.com/itsvixano-dev/android_vendor_firmware/-/tree/lineage-20/lisa) with `lisa` builds, there is no need to flash it.**

* Redmi S2/Y2 `ysl`: [`V12.0.2.0.PEFMIXM`](https://github.com/ItsVixano-releases/LineageOS_extra/releases/download/xiaomi_firmwares/fw_ysl_miui_HMS2Global_V12.0.2.0.PEFMIXM_668a87d737_9.0.zip)
* Redmi 6 Pro `sakura`: [`V12.0.1.0.PDICNXM`] `Due to bootloader locking issues caused by flashing the fw package, I won't provide the file as in, it's your fault if something happens on your phone.`
* Mi A2 Lite `daisy`: [`V11.0.21.0.QDLMIXM`](https://github.com/ItsVixano-releases/LineageOS_extra/releases/download/xiaomi_firmwares/fw_daisy_miui_DAISYGlobal_V11.0.21.0.QDLMIXM_c6721a4b58_10.0.zip)
* Xiaomi 11 Lite 5G NE `lisa`: [`V13.0.5.0.SKOMIXM`](https://github.com/ItsVixano-releases/LineageOS_extra/releases/download/xiaomi_firmwares/fw_lisa_miui_LISAGlobal_V13.0.5.0.SKOMIXM_3f2e54dd84_12.0.zip) `For Lineage 19.1`
* Xiaomi 11 Lite 5G NE `lisa`: [`V12.5.14.0.RKOMIXM`](https://github.com/ItsVixano-releases/LineageOS_extra/releases/download/xiaomi_firmwares/fw_lisa_miui_LISAGlobal_V12.5.14.0.RKOMIXM_6d3d47ddbb_11.0.zip) `For Lineage 18.1`

## Google Apps `GApps`

I highly suggest downloading [MindTheGapps](https://wiki.lineageos.org/gapps) if you want GMS on my LineageOS builds.

Download the correct package for your LineageOS installation:

**Please download the versions specified as the latest MindTheGapps version could cause some problems.**

* **Lineage 20.0**: [`MindTheGapps-13.0.0-arm64-20221025_100653.zip`](http://downloads.codefi.re/jdcteam/javelinanddart/gapps/old/MindTheGapps-13.0.0-arm64-20221025_100653.zip)
* **Lineage 19.1**: [`MindTheGapps-12.1.0-arm64-20220605_112439.zip`](http://downloads.codefi.re/jdcteam/javelinanddart/gapps/MindTheGapps-12.1.0-arm64-20220605_112439.zip)
* **Lineage 18.1**: [`MindTheGapps-11.0.0-arm64-20220217_100228.zip`](http://downloads.codefi.re/jdcteam/javelinanddart/gapps/MindTheGapps-11.0.0-arm64-20220217_100228.zip)

## Extras `Magisk, ...`

You can safely flash the [latest Magisk release](https://github.com/topjohnwu/Magisk/releases/latest) without any problems directly from `LineageOS Recovery`.

**WARNING: If you care about banking apps / others that contains root checks, don't flash Magisk since it will break device integrity. That can cause unwanted results such as missing apps on Google Play Store.**
