---
hide:
  - navigation
---

# Update / Upgrade

## Update (`19.1 may -> 19.1 june`)

My builds have support for OTA (Over-the-Air) updates.

To apply an update, go into `Settings -> System -> Updater`.

## Upgrade (`18.1 -> 19.1`)

The built-in updater **CANNOT** upgrade the existing installation.

To apply the upgrade, follow the [`Flashing`](../flashing/) page and skip the format data section.

## Tips
* Is advised to make regular backups of your data before updating, everything could go wrong, and I wouldn't be able to do a thing `¯\_(ツ)_/¯`.
* When upgrading, please flash the correct MindTheGapps version specified on the [`Download links`](../downloads/) according to the newer LineageOS version.
