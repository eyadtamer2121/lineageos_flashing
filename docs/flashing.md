---
hide:
  - navigation
---

# Flashing

## Preparation

Before starting, make sure you have downloaded all the necessary files mentioned on [`Download links`](../downloads/) page.

Place everything into a folder, and we can now start the magic :D.

## Notes
* No, you can't flash back TWRP or any other custom recoveries, overwise OTA updates would break.
* Follow every comment of this guide to avoid issues while you flash LineageOS.
* Don't continue if any of these steps fails.

## Flashing process

### LineageOS Recovery + Format Data
* First, reboot your device into fastboot mode by holding down the appropriate button combination (`Volume Down` + `Power`) until the word `FASTBOOT` appears on the screen.
* Open a `ADB & Fastboot tools` window on your PC and flash the `LineageOS Recovery` images you downloaded before.

``` bash
    # Mention the path of the images before running the commands (Mention the path of the images before running the command)
    # Ex: fastboot flash recovery /home/itsvixano/ysl/recovery.img
    fastboot flash recovery <recovery>.img # for `ysl` and `sakura`
    fastboot flash boot <boot>.img # for `daisy` and `lisa`
    fastboot flash vendor_boot <vendor_boot>.img # for `lisa`
    fastboot flash dtbo <dtbo>.img # for `lisa`
```

* Reboot your device into recovery mode by holding the appropriate button combination (`Volume Up` + `Power`) until `LineageOS Recovery` shows up on the screen.

### Format Data
**WARNING: Skip this step if you are upgrading to a newer LineageOS version.**

* On the screen, tap `Factory reset`, then `Format data/factory reset` and press `Format data`. This will delete all your device's data and remove encryption from an older ROM.
* Return to the main menu.

### Firmware Flash
**WARNING: Skip this step if you are flashing anything equal or newer than `LineageOS 20.0` on `lisa`.**

* Sideload the firmware zip we downloaded before (Ignore any `Signature verification failed` errors in this stage).

``` bash
    # On the device, Tap `Apply update`, then `Apply from ADB` for starting the sideload service
    # On the computer, sideload the package using this command (Mention the path of the firmware zip before running the command):
    # Ex: adb sideload /home/itsvixano/ysl/fw_ysl_miui_HMS2Global_V12.0.2.0.PEFMIXM_668a87d737_9.0.zip
    adb sideload <firmware>.zip
```

* Now reboot into recovery by tapping on the main menu `Advanced`, then `Reboot to recovery`.

### LineageOS Flash
* Sideload the LineageOS zip we downloaded before.

``` bash
    # On the device, Tap `Apply update`, then `Apply from ADB` for starting the sideload service
    # On the computer, sideload the package using this command (Mention the path of the LineageOS zip before running the command):
    # Ex: adb sideload /home/itsvixano/ysl/lineage-20.0-20221127-UNOFFICIAL-ysl.zip
    adb sideload <LineageOS>.zip
```

* Now reboot again into recovery by tapping on the main menu `Advanced`, then `Reboot to recovery`.

### Extras Flash (Gapps, Magisk, ...) if any
**WARNING: Gapps package MUST be flashed before booting into the OS, don't blame me if internet doesn't work on Play Store or something.**

* Sideload the remaining zip files (Gapps, Magisk, ...) (Ignore any `Signature verification failed` errors in this stage).

``` bash
    # On the device, Tap `Apply update`, then `Apply from ADB` for starting the sideload service
    # On the computer, sideload the package using this command (Mention the path of the extra zips before running the command):
    # Ex: adb sideload /home/itsvixano/ysl/Magisk.zip
    adb sideload <extra>.zip
```

### Final Reboot
* Now reboot into system by tapping on the main menu `Reboot system now`.
* If everything went well, you will be greeted by the infamous LineageOS boot animation :D.
